<!-- The Modal -->
      <div id="myModal" class="modal">
        <div class="row">
           
          <div class="col-md-6">
            <!-- Modal Content (The Image) -->
            <img class="modal-content" id="img01">
            <div id="input_stars" style="display: none">
              <form action="#" method="post">
          <div class="row">
            <div class="offset-md-4 offset-sm-3 offset-1">
            <div class="stars">
              <input class="star star-5 star-input" id="star-5" value="5" type="radio" name="note"/>
              <label class="star star-5 star-input" for="star-5"></label>
              <input class="star star-4 star-input" id="star-4" value="4" type="radio" name="note"/>
              <label class="star star-4 star-input" for="star-4"></label>
              <input class="star star-3 star-input" id="star-3" value="3" type="radio" name="note"/>
              <label class="star star-3 star-input" for="star-3"></label>
              <input class="star star-2 star-input" id="star-2" value="2" type="radio" name="note"/>
              <label class="star star-2 star-input" for="star-2"></label>
              <input class="star star-1 star-input" id="star-1" value="1" type="radio" name="note"/>
              <label class="star star-1 star-input" for="star-1"></label>
            </div>
          </div>
          </div>
      </form>
            </div>

            <div id="output_stars">

            </div>

           </div>


          <div class="col-md-6">
            <!-- Modal Caption (Image Text) On chargera les commentaires / Notation... ici -->
            <div id="caption">
              

              <div id="bloc-commentaire">
                <h1 class="text-center">Commentaires</h1>
                <div id="regroupement_commentaire">

                </div>

              </div>

              <div id="bloc-formulaire">
                <input type="hidden" id="id_image_commentaire">
                <?php if(isset($_SESSION['id_user'])){
                  echo '<form action="#" method="post" accept-charset="utf-8">
                <div class="container">
                  <div class="row">

                    <div class="col-md-12 offset-md-1">
                      <label for="comment">Votre commentaire:</label>
                      <textarea class="form-control" rows="5" id="texte"></textarea>
                    </div>

                    <div class="col-md-12 offset-3 offset-sm-4 offset-md-5">
                      <button type="submit" class="btn btn-success" id="envoi_commentaire">Envoyer</button>
                    </div>
                      <input type="hidden" name="submit" value="Commentaire">
                  </div>

                </div>
              </form>';
            }
            else{
              echo '<div class="container">
                  <div class="row">

                    <div class="col-md-12">
                      <h2 class="text-center" for="comment">Veuillez vous connecter pour commenter</h2>
                    </div>

                  </div>

                </div>';
            }
                  ?>
               
            </div>
              
              
             

            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-1 offset-md-11">
            <!-- The Close Button -->
            <span class="close">&times;</span>
          </div>
        </div>
      </div>


      <script src="./js/gestionImage.js"></script>
      <script src="./js/ajax/chargementImage.js"></script>
      <script src="./js/ajax/gereNote.js"></script>
      <script type="text/javascript" charset="utf-8" async defer>
        $(document).ready(function() {
        $('head').append('<!-- Etoiles notation -->');
        $('head').append('<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">');
        $('head').append('<link rel="stylesheet" href="./css/stars.css">');
        $('head').append('<link rel="stylesheet" href="./css/modal.css">');
        });
        
      </script>