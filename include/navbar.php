<?php
        $connected = (isset($_SESSION['id_user']))?1:0;
?>
  <link rel="stylesheet" href="css/background.css">

<div id="fadeconnect"></div>
<?php
        if($connected) {
          echo '<nav class="navbar navbar-expand-lg navbar-dark bg-dark">';
        }
        else {
          echo '<nav class="navbar navbar-dark bg-dark">';
        };
?>

    <a class="navbar-brand" href="index.php" id="nom_site">ZeroGram</a>
    <?php
    if ($connected)
      {
      echo '

      <button id= "hamburger" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
          ';
      }
    else
    {
      echo '<div id="login_unconnected">
            ';
    }
          ?>


        <?php

        if(!$connected) {
          $navbar_items = array
          (
          );
          echo '
          <ul class="navbar-nav mr-auto">';

                // Affiche les liens et ajoute la classe "active" pour la page courante
          foreach ($navbar_items as $key => $value)
          {
            echo '<li nav-item' . (basename($_SERVER['SCRIPT_NAME']) == $key ? ' class="active">' : '>') . '<a class="nav-link" href="' . $key . '">' . $value . '</a></li>';
          }
          echo '
          </ul>


          <button id="connection_navbar" class="btn btn-success">Login</button>

          ';

        }
        else{

          $navbar_items = array
          (
            'profil.php' => 'Mon Profil',
            '' => 'Controller'
          );
          echo '
          <ul class="navbar-nav mr-auto">';

                // Affiche les liens et ajoute la classe "active" pour la page courante
          foreach ($navbar_items as $key => $value)
          {
            echo '<li nav-item' . (basename($_SERVER['SCRIPT_NAME']) == $key ? ' class="active">' : '>') . '<a class="nav-link" id="' . $value . '" href="' . $key . '">';
            if($value === 'Controller')
                echo 'ZeroGram ' . $value . '</a></li>';
            else
                echo '' . $value . '</a></li>';
          }
          echo '
          <div id="errorController" style="display: none;"><center style="margin: 10px 10px; color: #ff3f3f; font-weight: bold;" >Échec de connexion...<br></center></div>

          </ul>
          <button id="deconnection" class="btn btn-danger">Leave</button>
          ';
        }
        ?>
    </div>
  </nav>

<?php
    if(!$connected){
?>
  <div id="connection">
    <h3>Se connecter</h3>
    <h5>Veuillez remplir tout les champs.</h5>
    <form action="./include/connexion.php" method="post" id="user-login">

      <input type="text" name="id" id="id" placeholder="Pseudo ou Email" required="" />
      <input type="password" name="pass" id="pass" placeholder="Mot de passe" required="" autocomplete="off"/>

      <div id="login_msg_pass" style="display: none;"><center style="margin: 10px 10px; color: #ff3f3f; font-weight: bold;" >Échec de connexion... Vérifiez vos identifiants<br></center></div>
      <div id="login_msg_pass4" style="display: none;"><center style="margin: 10px 10px; color: #63e9b1; font-weight: bold;" ><br></center></div>
      <!--Connexion réussie!-->
      <center id="create_account" style=" margin: 10px 10px;  font-weight: bold;" ><a href="#">Créer un compte ?</a></center>

      <button type="submit" id="coco">Connexion</button>
      <div id="close_button_connection" class="btn btn-success"><i class="material-icons">close</i></div>
      <input type="hidden" name="type_form" value="connection" />
    </form>
  </div>

  <div id="inscription">
    <h3>Inscription</h3>
    <h5>Veuillez remplir tout les champs.</h5>
    <form action="./include/inscription.php" method="post" id="user-inscription">
      <input type="text" class="input-sm" name="id2" id="id2" placeholder="Pseudo" required="" />
      <input type="email" class="input-sm" name="email" id="email" placeholder="E-mail" required="" />
      <input type="date" class="input-sm" name="birthday" id="birthday" placeholder="Date de naissance" required="" />
      <input type="password" class="input-sm" name="pass2" id="pass2" placeholder="Mot de passe" required="" autocomplete="off"/>
      <input type="password" class="input-sm" name="pass_confirm" id="pass_confirm" placeholder="Confirmer le mot de passe" required="" autocomplete="off"/>

      <div id="login_msg_pass2" style="display: none;"><center style="margin: 10px 10px; color: #ff3f3f; font-weight: bold;" >Échec de l'inscription... Vérifiez vos informations<br></center></div>
      <div id="login_msg_pass3" style="display: none;"><center style="margin: 10px 10px; color: #63e9b1; font-weight: bold;" >Inscription réussie!<br></center></div>

      <button type="submit" id="create">Créer</button>
      <div id="close_button_inscription" class="btn btn-success"><i class="material-icons">close</i></div>
      <input type="hidden" name="type_form" value="inscription" />
    </form>
  </div>
<?php
        }
?>
  <script src="./js/ajax/navbar.js"></script>
  <script>
    $('head').append('<link rel="stylesheet" type="text/css" href="./css/navbar.css">');
  </script>
