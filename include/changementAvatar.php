<?php

$db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');
if(isset($_GET['idUser']) && isset($_GET['idAvatar'])) {

    $stmt = $db->prepare('UPDATE utilisateurs SET id_avatar = :idAvatar WHERE id_utilisateur = :idUser');
    $stmt->bindParam(':idAvatar',$_GET['idAvatar']);
    $stmt->bindParam(':idUser',$_GET['idUser']);
    $stmt->execute();
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../profil.php">';
}

?>
