<?php
session_start();
$db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');
if(empty($_POST['id2']) || empty($_POST['email']) || empty($_POST['birthday']) || empty($_POST['pass2']) || empty($_POST['pass_confirm'])) {
    echo "KO";
} else if(isset($_POST['id2']) && isset($_POST['email']) && isset($_POST['birthday']) && isset($_POST['pass2']) && isset($_POST['pass_confirm'])) {
    if($_POST['pass2'] != $_POST['pass_confirm']) {
        echo "KO";
    } else {
        $stmt = $db->prepare("SELECT * FROM utilisateurs");
        $stmt->execute();
        $existPseudo = 0;
        $existEmail = 0;
        foreach ($stmt->fetchAll() as $donnees) {
            if(!strcmp($donnees['pseudo'],$_POST['id2']))
                $existPseudo = 1;
            if(!strcmp($donnees['email'],$_POST['email']))
                $existEmail = 1;
        }
        if($existPseudo || $existEmail) {
            echo "PseudoOrEmailAlreadyExist";
        } else {
            $stmt = $db->query("SELECT id_utilisateur FROM utilisateurs WHERE id_utilisateur = (SELECT max(id_utilisateur) FROM utilisateurs)");
            $donnees = $stmt->fetch();
            $nbAccount = $donnees['id_utilisateur'];
            $stmt = $db->prepare("INSERT INTO utilisateurs (id_utilisateur, pseudo, mot_de_passe, email, date_de_naissance, id_avatar, id_badge) VALUES (:id_utilisateur, :pseudo, :mot_de_passe, :email, :date_de_naissance, :id_avatar, :id_badge)");
            $idAccount = intval($nbAccount + 1);
            $stmt->bindParam(":id_utilisateur", $idAccount);
            $stmt->bindParam(":pseudo", $_POST['id2']);
            $stmt->bindParam(":mot_de_passe", $_POST['pass2']);
            $stmt->bindParam(":email", $_POST['email']);
            $stmt->bindParam(":date_de_naissance", $_POST['birthday']);
            $token = 1;
            $stmt->bindParam(":id_avatar", $token);
            $stmt->bindParam(":id_badge", $token);
            $stmt->execute();
            $stmt = $db->prepare("INSERT INTO a_pour_badge (id_utilisateur, id_badge) VALUES (:id_utilisateur, :id_badge)");
            $stmt->bindParam(":id_utilisateur", $idAccount);
            $stmt->bindParam(":id_badge", $token);
            $stmt->execute();
            echo "OK";
            // Badge Inscription
            $req2 = $db->prepare('INSERT INTO a_pour_badge (id_utilisateur,id_badge) VALUES(:id_utilisateur,1);');
            $req2->bindParam(':id_utilisateur',$_POST['id2']);
            $req2->execute();
        }
    }
} else {
    echo "KO";
}

?>
