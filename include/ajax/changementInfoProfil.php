<?php
session_start();
$db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');

$verif1 = "OK";
$verif2 = "OK";
$verif3 = "OK";

// Email
if(!empty($_POST['email']))
{
	if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
	{
		$stmt = $db->prepare("SELECT count(*) AS nbEmail FROM utilisateurs WHERE email = :email");
		$stmt->bindParam(":email", $_POST['email']);
		if($stmt->execute())
		{
			$donnees = $stmt->fetch();
			if(!$donnees['nbEmail'])
			{
				$stmt3 = $db->prepare('UPDATE utilisateurs SET email = :Email WHERE id_utilisateur = :idUser');
				$stmt3->bindParam(':Email',$_POST['email']);
				$stmt3->bindParam(':idUser',$_SESSION['id_user']);
				if($stmt3->execute())
		    		$verif1 = "OK";
		    	else
		    		$verif1 = "KO";
			}
			else
				$verif1 = "KO";
		}
		

	}
	else
		$verif1 = "KO";
}

// Mdp
if(!empty($_POST['pass2']) && !empty($_POST['pass2Confirm']))
{
	if(strcmp($_POST['pass2'],$_POST['pass2Confirm']))
	{
		$stmt4 = $db->prepare('UPDATE utilisateurs SET mot_de_passe = :Mdp WHERE id_utilisateur = :idUser');
	    $stmt4->bindParam(':Mdp',$_POST['pass2']);
	    $stmt4->bindParam(':idUser',$_SESSION['id_user']);
	    if($stmt4->execute())
	    	$verif2 = "OK";
	    else
			$verif2 = "KO";
	}
	else
		$verif2 = "KO";
}

// Pseudo
if(!empty($_POST['id2']))
{
	if(preg_match('`^([a-zA-Z0-9-_]{2,36})$`', $_POST['id2']))
	{
		$stmt = $db->prepare("SELECT count(*) AS nbPseudo FROM utilisateurs WHERE pseudo = :pseudo");
		$stmt->bindParam(":pseudo", $_POST['id2']);
		if($stmt->execute())
		{
			$donnees = $stmt->fetch();
			if(!$donnees['nbPseudo'])
			{
				$stmt = $db->prepare('UPDATE utilisateurs SET pseudo = :Pseudo WHERE id_utilisateur = :idUser');
		    	$stmt->bindParam(':Pseudo',$_POST['id2']);
		    	$stmt->bindParam(':idUser',$_SESSION['id_user']);
		    	if($stmt->execute())
		    		$verif3 = $_POST['id2'];
		    	else
		    		$verif3 = "KO";
			}
			else
				$verif3 = "KO";
		}
		

	}
	else
		$verif3 = "KO";
}

// Renvoi

if(!strcmp($verif1, $verif2) && !strcmp($verif1,"OK"))
{
	if(strcmp($verif3, "KO"))
	{
		echo $verif3;
	}
	else
		echo "KO";
}	
else
	echo "KO";
?>
