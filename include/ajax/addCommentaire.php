<?php
session_start();

if(isset($_SESSION['id_user']) && isset($_POST['texte']) && isset($_POST['id_image']))
{
	if($_POST['texte'] != ""){
		$db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram;charset=utf8', "zerogram", "zerogram");

	$requete = 'INSERT INTO a_pour_commentaire (id_utilisateur,id_image,commentaire)
				VALUES('.$_SESSION['id_user'].','.$_POST['id_image'].',"'. $_POST['texte'].'");';
	$stmp = $db->prepare($requete);
	$stmp->execute();

	$requete = 'SELECT * FROM a_pour_commentaire WHERE id_commentaire = (SELECT MAX(id_commentaire) AS id FROM a_pour_commentaire WHERE id_utilisateur = '.$_SESSION['id_user'].');';
	$stmp = $db->prepare($requete);
	$stmp->execute();
	$data = $stmp->fetch();
	// echo print_r($data);
	echo'<div class="container commentaire" id="'.$data['id_commentaire'].'">
			 	<div class="row">
			 	';
			 	// On récupère le badges et l'avatar de l'utilisateur pour ce commentaire
		 	$stmp2 = $db->prepare('
		 		SELECT badges.nom AS nom_badge, badges.chemin_image AS chemin_badge, avatar.chemin_image AS chemin_avatar,pseudo 
				FROM utilisateurs 
			    	INNER JOIN badges
			        	ON utilisateurs.id_badge = badges.id_badge
			        INNER JOIN avatar
			        	ON utilisateurs.id_avatar = avatar.id_avatar
			    WHERE id_utilisateur = ' . $_SESSION['id_user'] . ';');



    		$stmp2->execute();
    		$donnees_user = $stmp2->fetch();
    		echo '
			 	<span><img class="avatars-comment" src="'.$donnees_user['chemin_avatar'].'"></span>
                    <p class="pseudo">' .$donnees_user['pseudo']. '</p>
                    <date class="date">' .$data['date_commentaire']. '</date>
                    <span><img class="badge-comment" src="'.$donnees_user['chemin_badge'].'"></span>
                  </div>
                  <div class="row">
                    <div class="col-md-11 offset-md-1">
                      <p class="message-commentaire" id="' .$data['id_commentaire']. '">' .$_POST['texte']. ' </p>
                    </div>
                  </div>
                  <hr> 
                </div>';
	}
	else{
		echo 'KO';
	}
	
	
}
else{
	echo 'KO';
}
?>