<?php
session_start();
if(isset($_POST['id_image']))
{
	$db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram;charset=utf8', "zerogram", "zerogram");
	$requete = 'SELECT * FROM a_pour_commentaire INNER JOIN utilisateurs ON a_pour_commentaire.id_utilisateur = utilisateurs.id_utilisateur WHERE id_image = ' . $_POST['id_image'] . ';';
	$stmp = $db->prepare($requete);
	$stmp->execute();
	foreach ($stmp->fetchAll() as $donnees) {
		echo'<div class="container commentaire" id="'.$donnees['id_commentaire'].'">
			 	<div class="row">
			 	';
			 	// On récupère le badges et l'avatar de l'utilisateur pour ce commentaire
		 	$stmp2 = $db->prepare('
		 		SELECT badges.nom AS nom_badge, badges.chemin_image AS chemin_badge, avatar.chemin_image AS chemin_avatar 
				FROM utilisateurs 
			    	INNER JOIN badges
			        	ON utilisateurs.id_badge = badges.id_badge
			        INNER JOIN avatar
			        	ON utilisateurs.id_avatar = avatar.id_avatar
			    WHERE id_utilisateur = ' . $donnees['id_utilisateur'] . ';');

    		$stmp2->execute();
    		$donnees_user = $stmp2->fetch();
    		echo '
			 	<span><img class="avatars-comment" src="'.$donnees_user['chemin_avatar'].'"></span>
                    <p class="pseudo">' .$donnees['pseudo']. '</p>
                    <date class="date">' .$donnees['date_commentaire']. '</date>
                    <span><img class="badge-comment" src="'.$donnees_user['chemin_badge'].'"></span>
                  </div>
                  <div class="row">
                    <div class="col-md-11 offset-md-1">
                      <p class="message-commentaire" id="' .$donnees['id_commentaire']. '">' .$donnees['commentaire']. ' </p>
                    </div>
                  </div>
                  <hr> 
                </div>';
	}

}
else{
	echo 'KO';
}
?>