<?php

$db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');
$req = $db->prepare('SELECT * FROM badges WHERE id_badge = :id_badge;');
$req->bindParam(':id_badge',$_POST['id_badge']);
if($req->execute())
{
	$data = $req->fetchAll();
	echo json_encode($data);
}

?>