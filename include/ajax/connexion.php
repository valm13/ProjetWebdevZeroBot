<?php
$db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');
if(empty($_POST['id']) || empty($_POST['pass'])) {
    echo "KO";
} else if(isset($_POST['id']) && isset($_POST['pass'])) {
    $stmt = $db->prepare('SELECT id_utilisateur, pseudo, mot_de_passe FROM utilisateurs WHERE pseudo = :pseudo');
    $stmt->bindParam(':pseudo',$_POST['id']);
    $stmt->execute();
    $donnees = $stmt->fetch();
    if($donnees['pseudo'] == $_POST['id']) {
        if($donnees['mot_de_passe'] == $_POST['pass']) {
            session_start();
            $_SESSION['nom_user'] = $donnees['pseudo'];
            $_SESSION['id_user'] = $donnees['id_utilisateur'];
            echo $_POST['id'];
            // Badge Connexion
            $req2 = $db->prepare('INSERT INTO a_pour_badge (id_utilisateur,id_badge) VALUES(:id_utilisateur,2);');
            $req2->bindParam(':id_utilisateur',$_SESSION['id_user']);
            $req2->execute();
        } else {
            echo "KO";
        }
    } else {
        $stmt = $db->prepare('SELECT id_utilisateur, pseudo, email, mot_de_passe FROM utilisateurs WHERE email = :email');
        $stmt->bindParam(':email',$_POST['id']);
        $stmt->execute();
        $donnees = $stmt->fetch();
        if($donnees['email'] == $_POST['id']) {
            if($donnees['mot_de_passe'] == $_POST['pass']) {
                session_start();
                $_SESSION['nom_user'] = $donnees['pseudo'];
                $_SESSION['id_user'] = $donnees['id_utilisateur'];
                echo $donnees['pseudo'];
                // Badge Connexion
                $req2 = $db->prepare('INSERT INTO a_pour_badge (id_utilisateur,id_badge) VALUES(:id_utilisateur,2);');
                $req2->bindParam(':id_utilisateur',$_SESSION['id_user']);
                $req2->execute();
            } else {
                echo "KO";
            }
        } else {
            echo "KO";
        }
    }
} else {
    echo "KO";
}

?>
