<?php
session_start();
$retour = 'OK';
// id => nb_commentaire_mini
$valeurBadgesCom =  array(
	9  => 1,
	10 => 10,
	11 => 50,
);
$valeurBadgesnotes =  array(
	6 => 1,
	7 => 10,
	8 => 50,
);

$db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');
// Badges nombre de commentaires
if($_POST['type'] == 'commentaire'){
	$req = $db->prepare('SELECT id_badge FROM a_pour_badge WHERE id_utilisateur = :id_utilisateur;');
	$req->bindParam(':id_utilisateur',$_SESSION['id_user']);
	$req->execute();
	$alreadyBadge = $req->fetchAll();

	$req = $db->prepare('SELECT count(*) AS nombre FROM a_pour_commentaire WHERE id_utilisateur = :id_utilisateur;');
	$req->bindParam(':id_utilisateur',$_SESSION['id_user']);

	if($req->execute())
	{
		$data = $req->fetch();
		$nb = $data['nombre'];
		foreach ($valeurBadgesCom as $key => $value) 
		{
			if($nb >= $value && !in_array($key, $alreadyBadge))
			{
				$req2 = $db->prepare('INSERT INTO a_pour_badge (id_utilisateur,id_badge) VALUES(:id_utilisateur,:id_badge);');
				$req2->bindParam(':id_utilisateur',$_SESSION['id_user']);
				$req2->bindParam(':id_badge',$key);
				if($req2->execute())
				{
					$retour = 'OK_'.$key;
				}
			}
		}
		echo $retour;

	}
	else
		echo 'KO';
}
// Badge nombre de notes
else if($_POST['type'] == 'note'){
	$req = $db->prepare('SELECT id_badge FROM a_pour_badge WHERE id_utilisateur = :id_utilisateur;');
	$req->bindParam(':id_utilisateur',$_SESSION['id_user']);
	$req->execute();
	$alreadyBadge = $req->fetchAll();

	$req = $db->prepare('SELECT count(*) AS nombre FROM a_pour_note WHERE id_utilisateur = :id_utilisateur;');
	$req->bindParam(':id_utilisateur',$_SESSION['id_user']);

	if($req->execute())
	{
		$data = $req->fetch();
		$nb = $data['nombre'];
		foreach ($valeurBadgesnotes as $key => $value) 
		{
			if($nb >= $value && !in_array($key, $alreadyBadge))
			{
				$req2 = $db->prepare('INSERT INTO a_pour_badge (id_utilisateur,id_badge) VALUES(:id_utilisateur,:id_badge);');
				$req2->bindParam(':id_utilisateur',$_SESSION['id_user']);
				$req2->bindParam(':id_badge',$key);
				if($req2->execute())
				{
					$retour = 'OK_'.$key;
				}
			}
		}
		echo $retour;

	}
	else
		echo 'KO';
}
// Les badges Vous avez nb photo publié
else if($_POST['type'] == 'photo'){
	$valeurNbBadgesPhoto =  array(
		3 => 1,
		4 => 10,
		5 => 50,
	);

	$req = $db->prepare('SELECT id_badge FROM a_pour_badge WHERE id_utilisateur = :id_utilisateur;');
	$req->bindParam(':id_utilisateur',$_SESSION['id_user']);
	$req->execute();
	$alreadyBadge = $req->fetchAll();

	$req = $db->prepare('SELECT count(*) AS nombre FROM media WHERE id_utilisateur = :id_utilisateur AND public = 1;');
	$req->bindParam(':id_utilisateur',$_SESSION['id_user']);

	if($req->execute())
	{
		$data = $req->fetch();
		$nb = $data['nombre'];
		foreach ($valeurNbBadgesPhoto as $key => $value) 
		{
			if($nb >= $value && !in_array($key, $alreadyBadge))
			{
				$req2 = $db->prepare('INSERT INTO a_pour_badge (id_utilisateur,id_badge) VALUES(:id_utilisateur,:id_badge);');
				$req2->bindParam(':id_utilisateur',$_SESSION['id_user']);
				$req2->bindParam(':id_badge',$key);
				if($req2->execute())
				{
					$retour = 'OK_'.$key;
				}
			}
		}
		echo $retour;

	}
	else
		echo 'KO';
}
else{
	echo 'KO';
}
// Les badges Vous avez eu tant de badges
$valeurNbBadgesAcquis =  array(
	13 => 1,
	14 => 5,
	15 => 10,
);
$req = $db->prepare('SELECT id_badge FROM a_pour_badge WHERE id_utilisateur = :id_utilisateur;');
$req->bindParam(':id_utilisateur',$_SESSION['id_user']);
$req->execute();
$alreadyBadge = $req->fetchAll();

$req = $db->prepare('SELECT count(*) AS nombre FROM a_pour_badge WHERE id_utilisateur = :id_utilisateur;');
$req->bindParam(':id_utilisateur',$_SESSION['id_user']);

if($req->execute())
{
	$data = $req->fetch();
	$nb = $data['nombre'];
	foreach ($valeurNbBadgesAcquis as $key => $value) 
	{

		if($nb >= $value)
		{
			$req2 = $db->prepare('INSERT INTO a_pour_badge (id_utilisateur,id_badge) VALUES(:id_utilisateur,:id_badge);');
			$req2->bindParam(':id_utilisateur',$_SESSION['id_user']);
			$req2->bindParam(':id_badge',$key);
			$req2->execute();
		}
	}
}

// Badge ALL BADGE ?
// ON calcul le nombre de badge de l'utilisateur
$req = $db->prepare('SELECT count(id_badge) AS nombre FROM a_pour_badge WHERE id_utilisateur = :id_utilisateur;');
$req->bindParam(':id_utilisateur',$_SESSION['id_user']);
$req->execute();
$nbFromUser = $req->fetch();
// ON calcul le nombre de badge totaux en BDD
$req2 = $db->prepare('SELECT count(id_badge) AS nombre FROM badges;');
$req2->execute();
$enBDD = $req2->fetch();
		if($nbFromUser['nombre'] >= 2)
		{
			$req2 = $db->prepare('INSERT INTO a_pour_badge (id_utilisateur,id_badge) VALUES(:id_utilisateur,16);');
			$req2->bindParam(':id_utilisateur',$_SESSION['id_user']);
			$req2->execute();
		}
	

?>
