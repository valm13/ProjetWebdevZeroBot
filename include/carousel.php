<form action="#" method="post" accept-charset="utf-8">

    <div class="container">
      <div class="row">
		<h1 class="titre_type_photo">Photos Publiques</h1>
        <div class="col-md-12">
          <div class="withOptions">
            <?php 
            $db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram;charset=utf8', "zerogram", "zerogram");
            $stmp1 = $db->prepare("SELECT chemin_image,id_image FROM media WHERE id_utilisateur = :id_session AND public = 1");
            $stmp1->bindParam(':id_session',$_SESSION['id_user']);
            $stmp1->execute();
            $i = 0;
            foreach ($stmp1->fetchAll() as $donnees) {
              // print("<pre>".print_r($donnees,true)."</pre>");
              echo'<div class="col-xs-3 col-sm-3 col-md-3">
              <input name="'.$donnees['id_image']. '_public" type="checkbox" id="check_public'.$i.'">
              <label class="checkers" for="check_public'.$i.'"></label>
              <img id ="'.$donnees['id_image']. '" class="myImg" src="images/'.$donnees['chemin_image'].'">
              </div>  ';
              $i++;
            }
            ?>
          </div>
        </div>
      </div>
      <!-- Boutons entre les 2 carousels-->

      <div class="row">
        <div class="col-1 offset-3 offset-sm-4 offset-md-5"> 
          <a href="#"><button type="submit" id="switcher" class="btn btn-primary bouton-gestion-photo" value="switcher">SWITCHER</button></a>
        </div>
      </div>
      <div class="row" >
        <div class="col-1 offset-3 offset-sm-4 offset-md-5"> 
          <a href="#"><button  type="submit" id="supprimer" class="btn btn-primary bouton-gestion-photo" value="supprimer">SUPPRIMER</button></a>
        </div>
      </div>
      <input type="hidden" id="info_cacher" name="submit" value="">

      <div class="row">

        <div class="col-md-12">
          <div class="withOptions">
             <?php 
            $stmp1 = $db->prepare("SELECT chemin_image,id_image FROM media WHERE id_utilisateur = :id_session AND public = 0");
            $stmp1->bindParam(':id_session',$_SESSION['id_user']);
            $stmp1->execute();
            // print("<pre>".print_r($donnees,true)."</pre>");
            $i = 0;
            foreach ($stmp1->fetchAll() as $donnees) {
              echo'<div class="col-xs-3 col-sm-3 col-md-3">
              <input name="'.$donnees['id_image'].'_private" type="checkbox" id="check_private'.$i.'">
              <label class="checkers" for="check_private'.$i.'"></label>
              <img id ="'.$donnees['id_image']. '" class="myImg" src="images/'.$donnees['chemin_image'].'">
              </div>  ';
              $i++;
            }
            ?>
            </div>
          </div>
          <h1 class="titre_type_photo">Photos Privées</h1>
        </div>

      </div>
    </form>
    <script type="text/javascript">
      $('head').append('<!-- Slick Carousel -->');
      $('head').append('<link rel="stylesheet" type="text/css" href="carousel/slick-1.8.1/slick/slick.css"/>');
      $('head').append('<link rel="stylesheet" type="text/css" href="carousel/slick-1.8.1/slick/slick-theme.css"/>');
      $('head').append('<!-- My Carousel CSS -->');
      $('head').append('<link rel="stylesheet" type="text/css" href="./css/carousel.css"/>');
    </script>