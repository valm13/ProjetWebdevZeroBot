<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>ZeroGram !</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
          require_once('./util/require.php');
        ?>

        <!-- Feuilles de style -->
        <link rel="stylesheet" href="./css/styles.css">
        <link rel="stylesheet" href="./css/pageUtilisateur.css">
        <link rel="stylesheet" href="./css/all.css">
    </head>

    <header id="header" class="">
        <?php include('./include/navbar.php')?>
    </header>

    <body>
        <div id="fadeSelect"></div>
        <?php
            $db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');

            $id = (isset($_SESSION['id_user']))?(int) $_SESSION['id_user']:0;


            $stmp1 = $db->prepare("SELECT id_badge FROM utilisateurs WHERE id_utilisateur =:id_user");
            $stmp1->bindParam(':id_user',$_GET['idUser']);
            $stmp1->execute();
            $donnees1 = $stmp1->fetch();

            $stmp11 = $db->query('SELECT  nom, chemin_image, description FROM badges WHERE id_badge = '.$donnees1['id_badge']);
            $donneesBadge = $stmp11->fetch();

            $stmp12 = $db->query('SELECT  nom, chemin_image, description FROM badges WHERE id_badge = '.$donnees1['id_badge']);
            $donneesBadge = $stmp12->fetch();

            $stmp2 = $db->prepare("SELECT id_avatar FROM utilisateurs WHERE id_utilisateur =:id_user");
            $stmp2->bindParam(':id_user',$_GET['idUser']);
            $stmp2->execute();
            $donnees2 = $stmp2->fetch();

            $stmp21 = $db->query('SELECT chemin_image FROM avatar WHERE id_avatar = '.$donnees2['id_avatar'].'');
            $donnees21 = $stmp21->fetch();

            $stmp3 = $db->prepare("SELECT pseudo FROM utilisateurs WHERE id_utilisateur =:id_user");
            $stmp3->bindParam(':id_user',$_GET['idUser']);
            $stmp3->execute();
            $donnees3 = $stmp3->fetch();

            $stmp4 = $db->prepare("SELECT COUNT(*) AS nbImages FROM media WHERE id_utilisateur =:id_user AND public = 1");
            $stmp4->bindParam(':id_user',$_GET['idUser']);
            $stmp4->execute();
            $donnees4 = $stmp4->fetch();
            $nbImagesUtilisateur = $donnees4['nbImages'];

            $stmp5 = $db->prepare("SELECT chemin_image,id_image FROM media WHERE id_utilisateur =:id_user AND public = 1");
            $stmp5->bindParam(':id_user',$_GET['idUser']);
            $stmp5->execute();

             $stmp6 = $db->prepare("SELECT AVG(notation) AS noteUser FROM a_pour_note  WHERE id_utilisateur =:id_session GROUP BY id_utilisateur");
             $stmp6->bindParam(':id_session',$_GET['idUser']);
             $stmp6->execute();
             $donnees6 = $stmp6->fetch();
             $donnees7 = round($donnees6['noteUser'],1);

            echo('<div class="container" id="banniere_pseudo" >');
            echo('<div class="row">
                  <div class="col-md-2 offset-md-1 col-sm-2">
                  <div class="badge_texte">
                        <img class="image_badge_profil" data-toggle="tooltip" data-placement="left" title = "'.$donneesBadge['description'].'" alt="image du badges de l\'utilisateur" src='.$donneesBadge['chemin_image'].'>
                        <h1 class="texte_image_badge">'.$donneesBadge['nom'].'</h1>
                  </div>
                  </div>');

            echo('<div class="col-md-1 col-sm-1">
                        <img class="avatar" data-toggle="tooltip" data-placement="top" alt="image d\'avatar" src='.$donnees21['chemin_image'].'>
                  </div>');

            echo('<div class="col-md-1 col-sm-1">
                        <h1 id="pseudoUser" class="pseudo_utilisateur">'.$donnees3['pseudo'].'</h1>

                  </div>');

             echo('<div class="col-md-1 offset-md-2 col-sm-1">
                      <div class="etoileUser">
                       <img class="image_etoile" src="./img/etoile.PNG">
                      </div>
                        <h1 id="noteUser" class="note_utilisateur">'.$donnees7.'</h1>
                    </div>');


                  echo('</div>
                        </div>');

                        /*echo('<div class="row" id="ligne_separatrice">
                        <div class="col-md-5 col-sm-2 offset-md-4 offset-sm-0">
                        <div class="separateur bg-dark"></div>
                        </div>
                        </div>
                        </div>');*/
            

            echo('<div class="container">
                    <div class="row gallerieImage">
                      <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <h1 class="gallery-title">Galerie publique:</h1>
                      </div>');

            while($donnees5 = $stmp5->fetch()) {
                echo('
                  <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <img class="img-responsive myImg" id="'.$donnees5['id_image'].'" alt="images de l\'utilisateur" src="./images/'.$donnees5['chemin_image'].'">
                  </div>');
            }
            echo('</div>');
    ?>
    <?php include('./include/imageCliquable.php')?>
    <div class="row">
      <div class="alert alert-success offset-2 offset-md-2" id="success-alert">
        <h3>
          <img class="imageNewBadge" src="" alt="">
          <strong id="titre_accrocheur">Tadam vous avez gagner un badge !</strong>
          <img class="imageNewBadge" src="" alt="">
          </h3>
          <br>
          <p><strong class="alert_soustitre">Nom :</strong></p> <p id="alert_nom"></p>
          <p><strong class="alert_soustitre">Description :</strong></p> <p id="alert_description"></p>
          <h3>Vous pouvez l'activer dans votre profil</h3>
        </div>
        
      </div>
    </body>
</html>
