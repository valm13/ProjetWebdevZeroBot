<!DOCTYPE html>
<html lang="fr">

</head>

  
  <?php
  require_once('./util/require.php');
  ?>
  <!-- CSS -->
  <!-- Feuilles de style -->
  <link rel="stylesheet" type="text/css" href="./css/styles.css">
  <link rel="stylesheet" type="text/css" href="./css/profil.css">
  <link rel="stylesheet" href="./css/all.css">

</head>
<?php 
// print("<pre>".print_r($_SESSION)."</pre>");

// if(isset($_POST['message']))
if(isset($_SESSION['id_user']))
{
  if(isset($_POST))
  {
    if(isset($_POST['submit']))
    {
      $db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram;charset=utf8', "zerogram", "zerogram");
      // SUPPRESION D'IMAGE
      if($_POST['submit'] == 'supprimer')
      {
        $count['number'] = 0;
        $count['liste_id_image'] = array();
        foreach($_POST as $key => $value)
        {
          if(substr($value,0,2) == 'on'){
            // echo "<pre>".$key.' => '.$value."</pre>";
            $count['liste_id_image'][] = $key;
            $count['number']++;

          }

        }
        // print("<pre>".print_r($count)."</pre>");
        
        // $i = 0;
        $requete ='';
        foreach ($count['liste_id_image'] as $key => $value) {
          $id = substr($value,0,strpos($value, '_')); // On coupe l'id avant le "_"
          $requete .= 'DELETE FROM a_pour_commentaire WHERE id_image = ' . $id . ';';
          $requete .= 'DELETE FROM media WHERE id_utilisateur = ' . $_SESSION['id_user'] .' AND id_image = ' . $id . ';';
        }
        $stmp = $db->prepare($requete);
        $stmp->execute();

      }

      // SWITCHAGE D'IMAGE
      else if($_POST['submit'] == 'switcher')
      {
        $count['liste_id_image'] = array();
        foreach($_POST as $key => $value)
        {
          if(substr($value,0,2) == 'on'){
            $count['liste_id_image'][] = $key;
          }
        }
        // On récupère si c'est public ou privé comme
        $requete ='';
        foreach ($count['liste_id_image'] as $key => $value) {
          $id = substr($value,0,strpos($value, '_')); // On coupe l'id avant le "_"
          $visibility = substr($value,strpos($value, '_')+1); // On coupe après le "_" : public ou private 
          switch($visibility) 
          {
            case 'public':
              $requete .= 'UPDATE media SET public = 0 WHERE id_image = ' . $id . ';';
              break;

            case 'private':
              $requete .= 'UPDATE media SET public = 1 WHERE id_image = ' . $id . ';';
              break;

            default :

              break;
          }
         
        }
        $stmp = $db->prepare($requete);
        $stmp->execute();

        // print("<pre>".print_r($count)."</pre>");
      }
      
    }
  }
}

?>
<body>
  <header id="header" class="">
    <?php include('./include/navbar.php')?>
  </header>
  
<div id="fadeSelect"></div>
        <?php
            $db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');

            $id = (isset($_SESSION['id_user']))?(int) $_SESSION['id_user']:0;

            if($id == 0) {
                echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php">';
            }
            else {
                $stmp1 = $db->prepare("SELECT id_badge FROM utilisateurs WHERE id_utilisateur =:id_session");
                $stmp1->bindParam(':id_session',$id);
                $stmp1->execute();
                $donnees1 = $stmp1->fetch();

                $stmp11 = $db->query('SELECT  nom, chemin_image, description FROM badges WHERE id_badge = '.$donnees1['id_badge']);
                $donneesBadge = $stmp11->fetch();

                $stmp12 = $db->query('SELECT  nom, chemin_image, description FROM badges WHERE id_badge = '.$donnees1['id_badge']);
                $donneesBadge = $stmp12->fetch();

                $stmp2 = $db->prepare("SELECT id_avatar FROM utilisateurs WHERE id_utilisateur =:id_session");
                $stmp2->bindParam(':id_session',$id);
                $stmp2->execute();
                $donnees2 = $stmp2->fetch();


                $stmp21 = $db->query('SELECT chemin_image FROM avatar WHERE id_avatar = '.$donnees2['id_avatar'].'');
                $donnees21 = $stmp21->fetch();

                $stmp3 = $db->prepare("SELECT pseudo FROM utilisateurs WHERE id_utilisateur =:id_session");
                $stmp3->bindParam(':id_session',$id);
                $stmp3->execute();
                $donnees3 = $stmp3->fetch();

                $stmp28 = $db->prepare("SELECT AVG(notation) AS noteUser FROM a_pour_note  WHERE id_utilisateur =:id_session GROUP BY id_utilisateur");
                $stmp28->bindParam(':id_session',$id);
                $stmp28->execute();
                $donnees28 = $stmp28->fetch();
                $donnees50 = round($donnees28['noteUser'],1);


                echo('<div class="container" id="banniere_pseudo" >');
                echo('<div class="row">
                      <div class="col-md-2 offset-md-1 col-sm-2">
                      <div class="badge_texte">
                            <img class="image_badge_profil" data-toggle="tooltip" data-placement="left" title = "Cliquer pour changer de badge" alt="image du badges de l\'utilisateur" src='.$donneesBadge['chemin_image'].'>
                            <h1 class="texte_image_badge">'.$donneesBadge['nom'].'</h1>
                     </div>
                     </div>');

               echo('<div class="col-md-1 col-sm-1">
                            <img class="avatar" data-toggle="tooltip" data-placement="top" title = "Cliquer pour changer d\'avatar" alt="image d\'avatar" src='.$donnees21['chemin_image'].'>
                     </div>');

              echo('<div id="crayonEtPseudo"
              <div class="col-md-1 offset-md-1 col-sm-1">
                            <h1 id="pseudoUser" class="pseudo_utilisateur">'.$donnees3['pseudo'].'</h1>
                    </div>
                    <div class="col-md-1 offset-md-1 col-sm-1">
                           
                            <img class="image_crayon" title = "Modifier vos informations" alt="image du crayon" src="./img/pencil.png"></a>

                    </div>
                    <div class="col-md-1 col-sm-1">
                      <div class="etoileUser">
                      <img class="image_etoile" src="./img/etoile.PNG">
                        
                      </div>
                        <h1 id="noteUser" class="note_utilisateur">'.$donnees50.'</h1>
                    </div>
                    </div>');
                    echo('</div>
                          </div>
                          </div>');

                          /*echo('<div class="row" id="ligne_separatrice">
                          <div class="col-md-5 col-sm-2 offset-md-4 offset-sm-0">
                          <div class="separateur bg-dark"></div>
                          </div>
                          </div>
                          </div>');*/


                $stmp4 = $db->query("SELECT COUNT(*) AS nbBadge FROM badges");
                $donnees4 = $stmp4->fetch();
                $nbBadge = $donnees4['nbBadge'];

                $stmp9 = $db->prepare("SELECT COUNT(*) AS nbBadgeUtilisateur FROM a_pour_badge WHERE id_utilisateur = :id_session");
                $stmp9->bindParam(':id_session',$id);
                $stmp9->execute();
                $donnees9 = $stmp9->fetch();
                $nbBadgeUtilisateur = $donnees9['nbBadgeUtilisateur'];

                $stmp8 = $db->prepare("SELECT COUNT(*) AS nbBadgeUtilisateur FROM a_pour_badge WHERE id_utilisateur = :id_session");
                $stmp8->bindParam(':id_session',$id);
                $stmp8->execute();

                $stmp6 = $db->prepare("SELECT id_badge FROM a_pour_badge WHERE id_utilisateur = :id_session");
                $stmp6->bindParam(':id_session',$id);
                $stmp6->execute();
                $donnees6 = $stmp6->fetchAll();



                $stmp7 = $db->query("SELECT nom, chemin_image, description FROM badges WHERE id_badge = 17");
                $donnees7 = $stmp7->fetch();

                $stmp5 = $db->query("SELECT id_badge, nom, chemin_image, description FROM badges");

                echo('<div class="selection_badge">
                            <h1 class="titreSelectionBadge">Changer votre Badge</h1>');

                echo('<div class="row">');

                $tableauDeBadgeUtilisateur = [];

                for($m = 0; $m<17;$m++){
                  $tableauDeBadgeUtilisateur[$m] = 0;
                }

               for($p = 1;$p<17;$p++){
                 for($a = 0;$a<$nbBadgeUtilisateur;$a++){

                     if( $donnees6[$a]['id_badge'] == $p) {
                         $tableauDeBadgeUtilisateur[$p] = $donnees6[$a]['id_badge'];
                    }
                }
            }

            $i = 1;
            while($donnees5 = $stmp5->fetch()) {
                if($donnees5['id_badge'] != 17) {
                    $a = 0;

                if($donnees5['id_badge'] % 4 == 1) {
                    echo('</div>
                    <div class="row decallage">');
                }

                if( $tableauDeBadgeUtilisateur[$i] != 0){
                    echo('<div class="col-md-3 col-sm-2">
                          <div class="badge_texte-selection">
                                <a href="./include/changementBadge.php?idUser='.$id.'&amp;idBadge='.$donnees5['id_badge'].'"><img class="image_badge_profil_selection" id="'.$donnees5['id_badge'].'" title = "'.$donnees5['description'].'" alt="image du badges de l\'utilisateur" src='.$donnees5['chemin_image'].'></a>
                                <h1 class="texte_image_badge_selection">'.$donnees5['nom'].'</h1>
                          </div>
                          </div>');
                }
                else{
                    echo('<div class="col-md-3 col-sm-2">
                          <div class="badge_texte-selection">
                                <img class="image_badge_profil_selection" title = "'.$donnees7['description'].'" alt="image du badges de l\'utilisateur" src='.$donnees7['chemin_image'].'>
                                <h1 class="texte_image_badge_selection">'.$donnees7['nom'].'</h1>
                          </div>
                          </div>');
                }
                $i++;
            }
        }
        echo('</div>');
        echo('</div>');

        $stmp10 = $db->prepare("SELECT id_avatar, chemin_image FROM avatar");
        $stmp10->bindParam(':id_session',$id);
        $stmp10->execute();

        echo('<div class="selection_avatar">
                    <h1 class="titreSelectionAvatar">Changer votre Avatar</h1>');
                    echo('<div class="row">');

                    $i = 1;
                    while($donnees10 = $stmp10->fetch()) {
                        if($donnees10['id_avatar'] != 17) {


                        if($donnees10['id_avatar'] % 4 == 1) {
                            echo('</div>
                                  <div class="row decallage">');
                        }
                        echo('<div class="col-md-3 col-sm-2">
                              <div class="badge_texte-selection">
                                    <a href="./include/changementAvatar.php?idUser='.$id.'&amp;idAvatar='.$donnees10['id_avatar'].'"><img class="image_avatar_profil_selection" id="'.$donnees10['id_avatar'].'"  alt="image des avatars" src='.$donnees10['chemin_image'].'></a>
                              </div>



                              </div>');
                        }

                        $i++;
                    }

                    echo('
                    </div>
                    </div>
                        <div id="maj">
                          <h3>Modification des informations</h3>
                          <form id="user-modif-profil">
                            <input type="text" class="input-sm" name="id2" id="id2" placeholder="Pseudo"/>
                            <input type="text" class="input_idUser" name="idUser" id="idUser" value="'.$id.'"/>
                            <input type="email" class="input-sm" name="email" id="email" placeholder="E-mail"/>
                            <input type="password" class="input-sm" name="pass2" id="pass2" placeholder="Mot de passe" autocomplete="off"/>
                            <input type="password" class="input-sm" name="pass2Confirm" id="pass2Confirm" placeholder="Confirmation du Mot de Passe" autocomplete="off"/>

                            <div id="msg_erreur_modif_profil" style="display: none;"><center style="margin: 10px 10px; color: #ff3f3f; font-weight: bold;" >Échec de modification... Vérifiez vos informations<br></center></div>
                            <div id="msg_pass" style="display: none;"><center style="margin: 10px 10px; color: #63e9b1; font-weight: bold;" >Vos informations on été correctement modifiées!<br></center></div>

                            <button type="submit" id="modify">Modifier mes données</button>
                          </form>
                        </div> ');

                                    $i++;
                }
    ?>
    <?php include('./include/carousel.php');?>
      
    <?php include('./include/ImageCliquable.php');?>

    <!-- Carousel -->
    <script type="text/javascript" src="carousel/slick-1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript" src="./js/carousel.js"></script>

    <!-- Info du profil -->
    <script type="text/javascript" src="./js/ajax/modifProfil.js"></script>
    <div class="row">
      <div class="alert alert-success offset-2 offset-md-2" id="success-alert">
        <h3>
          <img class="imageNewBadge" src="" alt="">
          <strong id="titre_accrocheur">Tadam vous avez gagner un badge !</strong>
          <img class="imageNewBadge" src="" alt="">
          </h3>
          <br>
          <p><strong class="alert_soustitre">Nom :</strong></p> <p id="alert_nom"></p>
          <p><strong class="alert_soustitre">Description :</strong></p> <p id="alert_description"></p>
          <h3>Vous pouvez l'activer dans votre profil</h3>
        </div>
        
      </div>
  </body>

</html>

