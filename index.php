<!DOCTYPE html>
<html lang="fr">
  <head>
    <title>ZeroGram</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
      require_once('./util/require.php');
    ?>
    <!-- Feuilles de style -->
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" href="./css/all.css">
  </head>

 <header id="header" class="">
    <?php include('./include/navbar.php')?>
</header>

  <body>
        <?php
            $db = new PDO('mysql:host=mysql-zerogram.alwaysdata.net;dbname=zerogram_zerogram', 'zerogram', 'zerogram');

            $stmp0 = $db->query("SELECT COUNT(id_utilisateur) AS nbUser FROM utilisateurs");
            $donnees0 = $stmp0->fetch();
            $nbUtilisateurs = $donnees0['nbUser'];

            $stmp1 = $db->query("SELECT  id_badge, pseudo, id_avatar, id_utilisateur, COUNT(id_image) AS count FROM utilisateurs INNER JOIN media USING(id_utilisateur) WHERE public = 1 GROUP BY id_utilisateur ORDER BY count DESC");

            foreach ($stmp1->fetchAll() as $donnees1) {

                $stmp12 = $db->query('SELECT  nom, chemin_image, description FROM badges WHERE id_badge = '.$donnees1['id_badge']);
                $donneesBadge = $stmp12->fetch();

                $stmp21 = $db->query('SELECT chemin_image FROM avatar WHERE id_avatar = '.$donnees1['id_avatar']);
                $donnees21 = $stmp21->fetch();

                $stmp3 = $db->prepare("SELECT id_image, chemin_image FROM media WHERE id_utilisateur =:id_user AND public = 1");
                $stmp3->bindParam(':id_user',$donnees1['id_utilisateur']);
                $stmp3->execute();
                $donnees3 = $stmp3->fetchAll();

                $stmp4 = $db->prepare("SELECT COUNT(*) AS nbImageUser FROM media WHERE id_utilisateur =:id_user AND public = 1");
                $stmp4->bindParam(':id_user',$donnees1['id_utilisateur']);
                $stmp4->execute();
                $donnees4 = $stmp4->fetch();
                $nbImageUtilisateur = $donnees4['nbImageUser'];
                $nbImageUtilisateurNonAffiche = $nbImageUtilisateur - 3;

                 $stmp5 = $db->prepare("SELECT AVG(notation) AS noteUser FROM a_pour_note  WHERE id_utilisateur =:id_session GROUP BY id_utilisateur");
                $stmp5->bindParam(':id_session',$donnees1['id_utilisateur']);
                $stmp5->execute();
                $donnees5 = $stmp5->fetch();
                $donnees6 = round($donnees5['noteUser'],1);

                if(isset($donnees3[0]['chemin_image'])) {
                      echo('<div class="container" id="banniere_pseudo" >');
                      echo('<div class="row">
                            <div class="col-md-2 offset-md-4 col-sm-2">
                            <div class="badge_texte">
                                  <a href="./utilisateur.php?idUser='.$donnees1['id_utilisateur'].'"><img class="image_badge_profil" data-toggle="tooltip" data-placement="left" title = "'.$donneesBadge['description'].'" alt="image du badges de l\'utilisateur" src='.$donneesBadge['chemin_image'].'></a>
                                  <h1 class="texte_image_badge">'.$donneesBadge['nom'].'</h1>
                            </div>
                            </div>');

                      echo('<div class="col-md-1 col-sm-1">
                            <a href="./utilisateur.php?idUser='.$donnees1['id_utilisateur'].'"><img class="avatar" data-toggle="tooltip" data-placement="top" alt="image d\'avatar" src='.$donnees21['chemin_image'].'></a>
                            </div>');

                      echo('<div class="col-md-1 col-sm-1">
                            <a href="./utilisateur.php?idUser='.$donnees1['id_utilisateur'].'"><h1 id="pseudoUser" class="pseudo_utilisateur">'.$donnees1['pseudo'].'</h1></a>
                            </div>');

                      echo('<div class="col-md-1 offset-md-2 col-sm-1">
                      <div class="etoileUser">
                       <img class="image_etoile" src="./img/etoile.PNG">
                      </div>
                        <h1 id="noteUser" class="note_utilisateur">'.$donnees6.'</h1>
                    </div>');

                      echo('</div>
                            </div>');
                        }

                        if(isset($donnees3[3]['chemin_image'])) {
                            echo('<div class="row" id="image_utilisateur_index">
                                  <div class="col-md-2 col-sm-2 offset-md-2 offset-sm-0">
                                        <img  id ="'.$donnees3[0]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[0]['chemin_image'].'"">
                                  </div>
                                  <div class="col-md-2 col-sm-2">
                                        <img id ="'.$donnees3[1]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[1]['chemin_image'].'"">
                                  </div>
                                  <div class="col-md-2 col-sm-2">
                                        <img id ="'.$donnees3[2]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[2]['chemin_image'].'"">
                                  </div>
                                  <div class="col-md-2 col-sm-2">
                                        <a href="./utilisateur.php?idUser='.$donnees1['id_utilisateur'].'"><img class="img-responsive myImg" alt="images de l\'utilisateur" src=./images/noir.jpg></a>
                                        <a href="./utilisateur.php?idUser='.$donnees1['id_utilisateur'].'"><h1 id="texte_nb_image_compte_restant"> '.$nbImageUtilisateurNonAffiche.' + </h1></a>
                                  </div>
                                  </div>');
                        }
                        else if(isset($donnees3[2]['chemin_image'])) {
                            echo('<div class="row" id="image_utilisateur_index">
                                  <div class="col-md-2 col-sm-2 offset-md-3 offset-sm-0">
                                        <img id ="'.$donnees3[0]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[0]['chemin_image'].'"">
                                  </div>
                                  <div class="col-md-2 col-sm-2">
                                        <img id ="'.$donnees3[1]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[1]['chemin_image'].'"">
                                  </div>
                                  <div class="col-md-2 col-sm-2">
                                        <img id ="'.$donnees3[2]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[2]['chemin_image'].'"">
                                  </div>
                                  </div>');
                        }
                        else if(isset($donnees3[1]['chemin_image'])) {
                            echo('<div class="row" id="image_utilisateur_index">
                                  <div class="col-md-2 col-sm-2 offset-md-4 offset-sm-0">
                                        <img id ="'.$donnees3[0]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[0]['chemin_image'].'"">
                                  </div>
                                  <div class="col-md-2 col-sm-2">
                                        <img id ="'.$donnees3[1]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[1]['chemin_image'].'"">
                                  </div>
                                  </div>');
                        }
                        else if(isset($donnees3[0]['chemin_image'])) {
                            echo('<div class="row" id="image_utilisateur_index">
                                  <div class="col-md-2 col-sm-2 offset-md-5 offset-sm-0">
                                        <img id ="'.$donnees3[0]['id_image'].'" class="img-responsive myImg" alt="images de l\'utilisateur" src="./images/'.$donnees3[0]['chemin_image'].'"">
                                  </div>
                                  </div>');
                        }
            }
    ?>
    <?php include('./include/imageCliquable.php')?>

    <div class="row">
      <div class="alert alert-success offset-2 offset-md-2" id="success-alert">
        <h3>
          <img class="imageNewBadge" src="" alt="">
          <strong id="titre_accrocheur">Tadam vous avez gagner un badge !</strong>
          <img class="imageNewBadge" src="" alt="">
          </h3>
          <br>
          <p><strong class="alert_soustitre">Nom :</strong></p> <p id="alert_nom"></p>
          <p><strong class="alert_soustitre">Description :</strong></p> <p id="alert_description"></p>
          <h3>Vous pouvez l'activer dans votre profil</h3>
        </div>
        
      </div>
    
  </body>
</html>
