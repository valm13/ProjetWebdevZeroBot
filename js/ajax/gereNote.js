$(document).ready(function() {

// Get the modal
var modal = $('#myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = $('.myImg');
var modalImg = $('#img01');
var captionText = $('caption');
var span = $( ".close" );

/*Fonctions AJAX*/

  function gereAffichage(id){
    $.post(
    './include/ajax/showOrHide.php', // Le fichier cible côté serveur.
    {
        id_image : id // Nous supposons que ce formulaire existe dans le DOM.
      },
    fonction_retour_affichage, // Nous renseignons uniquement le nom de la fonction de retour.
    'text' // Format des données reçues.
    );

    function fonction_retour_affichage(texte_recu){
      if(texte_recu == "SHOW")
      {
        $("#input_stars").show();
        document.getElementById("star-1").checked = false;
        document.getElementById("star-2").checked = false;
        document.getElementById("star-3").checked = false;
        document.getElementById("star-4").checked = false;
        document.getElementById("star-5").checked = false;
      }
      else if(texte_recu == "HIDE")
      {
        console.log('HIDE');
        $("#input_stars").hide();
        renvoieNote();
      }
    }
  }

  function renvoieNote(){
   
    $.post(
    './include/ajax/renvoieNote.php', // Le fichier cible côté serveur.
    {
        id_image : $('#id_image_commentaire').attr('value') // Nous supposons que ce formulaire existe dans le DOM.
      },
    fonction_retour_renvoie, // Nous renseignons uniquement le nom de la fonction de retour.
    'text' // Format des données reçues.
    );

    function fonction_retour_renvoie(texte_recu){
      if(texte_recu != "KO")
      {
        console.log('On affiche la note finale');
        $("#input_stars").hide();
        $("#output_stars").append(texte_recu);
        document.getElementById("star-5-dis").disabled = true;
        document.getElementById("star-4-dis").disabled = true;
        document.getElementById("star-3-dis").disabled = true;
        document.getElementById("star-2-dis").disabled = true;
        document.getElementById("star-1-dis").disabled = true;
      }
    }
  }

    function rentreNote(etoileValue){
    $.post(
    './include/ajax/rentreNote.php', // Le fichier cible côté serveur.
    {
        id_image : $('#id_image_commentaire').attr('value'),
         note : etoileValue
      },
    fonction_retour_rentre, // Nous renseignons uniquement le nom de la fonction de retour.
    'text' // Format des données reçues.
    );

    function fonction_retour_rentre(texte_recu){
      if(texte_recu == "OK")
      {
        renvoieNote();
        badge('note');
      }
    }
  }

  function enleveNote(){
    $("#output_stars").empty();
  }


  

/*Evenements*/

  // Lors du clic sur une image on génère les commentaires et on affiche l'image
  img.click(function(){
    gereAffichage($(this).attr('id'));
  });

  $('.star-input').click(function(){
    rentreNote($(this).attr('value'));
  });

  span.click(function() {
    enleveNote();
  });

});