$(document).ready(function() {

// Get the modal
var modal = $('#myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = $('.myImg');
var modalImg = $('#img01');
var captionText = $('caption');
var span = $( ".close" );
$("#success-alert").hide();

/*Fonctions AJAX*/

  // Genere les commentaires
  function genereCommentaire(id){
    $.post(
    './include/ajax/getCommentaire.php', // Le fichier cible côté serveur.
    {
        id_image : id // Nous supposons que ce formulaire existe dans le DOM.
      },
    fonction_retour, // Nous renseignons uniquement le nom de la fonction de retour.
    'text' // Format des données reçues.
    );

    function fonction_retour(texte_recu){
      if(texte_recu != "KO")
      {
        $("#regroupement_commentaire").append(texte_recu);
      }
      else
      {
        // alert('Error :\n texte_recu =\n'+texte_recu);
      }
    }
  }

  // Vide les commentaires du contenu
  function enleverCommentaire(){
   if ($("#regroupement_commentaire") !== null)
   {
     $("#regroupement_commentaire").empty();
   }
 }

  // Ajoute le commentaire à la bdd et l'affiche dans les commentaires
  function addCommentaire(event){
  event.preventDefault(); // Pour bloquer le rechargement de la page

  $.post(
    './include/ajax/addCommentaire.php', // Le fichier cible côté serveur.
    {
      id_image : $('#id_image_commentaire').attr('value'),
      texte : $("#texte").val() // Nous supposons que ce formulaire existe dans le DOM.
    },
    fonction_retour, // Nous renseignons uniquement le nom de la fonction de retour.
    'text' // Format des données reçues.
    );

  function fonction_retour(texte_recu){
    if(texte_recu != "KO")
    {
      $("#regroupement_commentaire").append(texte_recu);
      $('#texte').val('');
      badge('commentaire');
    }
    else
    {
      // alert('Error :\n'+texte_recu);
    }
  }
}


/*Evenements*/

// Lors d'un clic, si on le fait sur le div myModal on ferme l'image
$(document).click(function (event) {
  var clickover = $(event.target);
  console.log(clickover);
  if(clickover[0] == modal[0]){
    modal.fadeOut();
    enleverCommentaire()
  }
});
  // Lors du clic sur une image on génère les commentaires et on affiche l'image
  img.click(function(){
    genereCommentaire($(this).attr('id'));
    modal.fadeIn();
    modalImg.attr("src",this.src);
    captionText.html(this.alt);
    $('#id_image_commentaire').attr('value', $(this).attr('id'));
  });

  // Lors du clic sur le close d'une image --> On ferme le modal et on vide les commentaires
  span.click(function() {
    modal.fadeOut();
    enleverCommentaire();

  });

  // Lorsqu'on envois le commentaire, on appelle la fonction addCommentaire
  $('#envoi_commentaire').click(function(event){
    // $('#texte').val($.trim($(this).val()));
    addCommentaire(event);
  });

});