function showAlert() {
    $("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
     $("#success-alert").slideUp(500);
   });
}       

function badge(type_badge){
  $.post(
    './include/ajax/attribueBadge.php',
    {
      type : type_badge,
    },
    function(data) {
      if(data != 'KO') {
        console.log(data);
        if(data.length > 2) // on a un nouveau badge
        {
          console.log(data);
          //Badge de la forme : OK_idBadge
          var badge = data.slice(3);
          // Appel fonction afficheNewBadge qui fait apparaitre une popup avec le nouveau badge pendant 5s.
          // Elle prend en paramètre l'id du badge et fais la requete renvoyant un fichier Json:
          if($.isNumeric(badge)){
            console.log('nouveau badge :'+badge);
            afficherBadge(badge); 
          }
          
          

        }
        else // Tout c'est bien passé mais on n'a pas débloqué de badge
        {
          console.log('t\'as deja les badges');
        }
        
      } else {
        console.log('La requete compte commentaire n\'a pas fonctionné');
      }
    },
    );
}

function afficherBadge(id){
  $.ajax({
            type:'POST',
            url:'./include/ajax/returnBadgeJson.php',
            dataType: "json",
            data:{id_badge:id},
            success:function(data){
              if(data[0].nom !== "undefined"){
                console.log('success');
              console.log(data);

              $('#alert_nom').text(data[0].nom);
              $('#alert_description').text(data[0].description);
              $('.imageNewBadge').attr({src:data[0].chemin_image, alt:data[0].description});
              console.log('ajout : fait');
              showAlert();
              }
              
            }
        });
}