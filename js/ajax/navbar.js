$(document).ready(function() {

    /****** NAVBAR *****/

    // On ferme la navbar si on clique à coté
    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $("#navbarSupportedContent").hasClass("navbar-collapse collapse show");
        if (_opened === true && !clickover.hasClass("navbar-toggler")) {
            $("#hamburger").click();
        }
    });

    /****** CONNECT/INSCRIPTION *****/

    var panelConnection = $('#connection');
    var panelInscription = $('#inscription');
    var fadeConnect = $('#fadeconnect');
    var errorConnexion = $('#login_msg_pass');
    var errorInscription = $('#login_msg_pass2');
    var successInscription = $('#login_msg_pass3');
    var successConnexion = $('#login_msg_pass4');
    var connectSite = $('.connect');
    var disconnectSite = $('.disconnect');
    var errorController = $('#errorController');

    function showForm(form) {
        console.log("Showing "+form.attr('id')+" form");
        form.fadeIn();
        fadeConnect.fadeIn();
    }

    function showFormWithoutFade(form) {
        console.log("Showing "+form.attr('id')+" form");
        form.fadeIn();
    }

    function hideForm(form,value) {
        console.log("Hiding "+form.attr('id')+" form");
        form.fadeOut();
        if(value != true)
            fadeConnect.fadeOut();
    }

    $('#coco').click(function(e) {
        e.preventDefault();
        $.post(
            './include/ajax/connexion.php',
            {
                id : $('#id').val(),
                pass : $('#pass').val()
            },
            function(data) {
                if(data === 'KO') {
                    showForm(errorConnexion);
                    setTimeout(function() {hideForm(errorConnexion, true)}, 2000);
                } else {
                    var fieldNameElement = document.getElementById('login_msg_pass4');
                    fieldNameElement.style.margin = "10px 10px";
                    fieldNameElement.style.color = "#63e9b1";
                    fieldNameElement.style.fontWeight = "bold";
                    fieldNameElement.style.textAlign = "center";
                    fieldNameElement.innerHTML = "Bienvenue " + data + "!";
                    showForm(successConnexion);
                    setTimeout(function() {hideForm(successConnexion, true)}, 2000);
                    setTimeout(function() {location.reload()}, 2500);
                }
            },
        );
    });

    $('#create').click(function(e) {
        e.preventDefault();
        $.post(
            './include/ajax/inscription.php',
            {
                id2 : $('#id2').val(),
                email : $('#email').val(),
                birthday : $('#birthday').val(),
                pass2 : $('#pass2').val(),
                pass_confirm : $('#pass_confirm').val()
            },
            function(data) {
                if(data === 'OK') {
                    showForm(successInscription);
                    setTimeout(function() {hideForm(successInscription, true)}, 2000);
                    setTimeout(function() {hideForm(panelInscription, true)}, 2500);
                    setTimeout(function() {showForm(panelConnection)}, 2500);
                } else {
                    alert(data);
                    showForm(errorInscription);
                    setTimeout(function() {hideForm(errorInscription, true)}, 2000);
                }
            },
        );
    });

    $('#fadeconnect').click(function(e) {
        e.preventDefault();
        hideForm(panelConnection);
        hideForm(panelInscription);
    });

    $('#deconnection').click(function(e) {
        e.preventDefault();
        $.post(
            './include/ajax/deconnexion.php',
        );
        location.reload();
    });

    $('#Controller').click(function(e) {
        e.preventDefault();
        $.post(
            './include/ajax/controller.php',
            function(data) {
                if(data === 'OK') {
                    location.href="http://172.20.10.13:3000";
                } else {
                    alert(data);
                    showFormWithoutFade(errorController);
                    setTimeout(function() {hideForm(errorController, false)}, 2000);
                }
            },
        );
    });

    /****** CONNECTION *****/
    $('#connection_navbar').click(function() {
        if(!panelConnection.is(':visible') && !panelInscription.is(':visible'))
        showForm(panelConnection);
    });

    $('#close_button_connection').click(function() {
    	hideForm(panelConnection);
    });

    /****** INSCRIPTION *****/
    $('#create_account').click(function() {
        showForm(panelInscription);
        hideForm(panelConnection,true);
    });

    $('#close_button_inscription').click(function() {
        hideForm(panelInscription);
    });

    /*Press Echap*/
    $(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
     	if( panelConnection.is(':visible')){
     		hideForm(panelConnection);
     	}
        if (panelInscription.is(':visible')){
            hideForm(panelInscription);
        }
    }
    });
});
