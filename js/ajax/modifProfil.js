$(document).ready(function() {
    /****** Variables *****/

    var panelModificationProfil = $('#maj');
    var errorModifProfil = $('#msg_erreur_modif_profil');
    var passModifProfil = $('#msg_pass');
    var fadeSelect = $('#fadeSelect');
    var fadeconnect = $('#fadeconnect');

    /*Fonctions Simples*/

    function cacher(){
        fadeSelect.fadeOut();
        fadeconnect.fadeOut();
    }

    /*Fonctions AJAX*/

    $('#modify').click(function(e) {
        e.preventDefault();
        $.post(
            './include/ajax/changementInfoProfil.php',
            {
                id2 : $('#id2').val(),
                email : $('#email').val(),
                pass2 : $('#pass2').val(),
                pass2Confirm : $('#pass2Confirm').val()
            },
            function(data) {
                if(data === 'OK'){
                    passModifProfil.fadeIn();
                    setTimeout(function() {passModifProfil.fadeOut()}, 2000);
                    setTimeout(function() {panelModificationProfil.fadeOut()}, 2500);
                    setTimeout(function() {
                        document.getElementById("id2").value = "";
                        document.getElementById("email").value = "";
                        document.getElementById("pass2").value = "";
                        document.getElementById("pass2Confirm").value = "";
                    }, 2500);
                    setTimeout(function() {cacher()}, 2500);
                } else if(data === 'KO') {

                    errorModifProfil.fadeIn();
                    setTimeout(function() {errorModifProfil.fadeOut()}, 2000);
                    setTimeout(function() {panelModificationProfil.fadeOut()}, 2500);
                    setTimeout(function() {
                        document.getElementById("id2").value = "";
                        document.getElementById("email").value = "";
                        document.getElementById("pass2").value = "";
                        document.getElementById("pass2Confirm").value = "";
                    }, 2500);
                    setTimeout(function() {cacher()}, 2500);
                } else if(data != '') {
                    // On a changé le pseudo
                    passModifProfil.fadeIn();
                    setTimeout(function() {passModifProfil.fadeOut()}, 2000);
                    setTimeout(function() {panelModificationProfil.fadeOut()}, 2500);
                    setTimeout(function() {
                        document.getElementById("id2").value = "";
                        document.getElementById("email").value = "";
                        document.getElementById("pass2").value = "";
                        document.getElementById("pass2Confirm").value = "";
                    }, 2500);
                    setTimeout(function() {
                        cacher();
                        //document.getElementById("pseudoUser").value = data;
                        document.getElementById("pseudoUser").innerHTML = data;
                    }, 2500);

                }


            },
            );
    });
    
    /*Evenements*/    

    $('.image_badge_profil').click(function(){
        console.log('click badge');
        var fadeSelect = $('#fadeSelect');
        var panel = $('.selection_badge');
        if(!panel.is(':visible')){
            panel.fadeIn();
            fadeSelect.fadeIn();
        }
    });

    $('.avatar').click(function(){
        console.log('click avatar');
        var fadeSelect = $('#fadeSelect');
        var panel = $('.selection_avatar');
        if(!panel.is(':visible')){
          panel.fadeIn();
          fadeSelect.fadeIn();
      }
  });

    $('.image_crayon').click(function(){
        console.log('click crayon');
        var fadeSelect = $('#fadeSelect');
        var panel = $('#maj');
        if(!panel.is(':visible')){
          panel.fadeIn();
          fadeSelect.fadeIn();
      }
  });

    $('#fadeSelect').click(function(){
        var panel = $('#fadeconnect');
        var avatar =  $('.selection_avatar');
        var badge = $('.selection_badge');
        var modifProfil = $('#maj');

        panel.fadeOut();
        avatar.fadeOut();
        badge.fadeOut();
        modifProfil.fadeOut();
        $(this).fadeOut();
    });


});
