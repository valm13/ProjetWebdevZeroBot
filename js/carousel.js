$(document).ready(function() {

  /*Carrousel Options*/

  $('.withOptions').slick({
    arrow:true,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        adaptiveHeight: true,
        infinite: false,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        adaptiveHeight: true,
        slidesToScroll: 2,
        infinite: false,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        adaptiveHeight: true,
        slidesToScroll: 1,
        infinite: false,
      }
    }
    ]
  });

});