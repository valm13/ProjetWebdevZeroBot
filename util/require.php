<?php
// Dossier de travail (à changer selon l'hébergement)
// !! ATTENTION !!
// Il faut activer l'option allow_url_include sur PHP

$_DOSSIER_TRAVAIL = '/';			// Chemin à modifier pour correspondre à son dossier wamp

$_WORK_PATH_PHP = $_SERVER['DOCUMENT_ROOT'] . $_DOSSIER_TRAVAIL;			// Chemin à utiliser pour accéder à des fichiers en PHP
$_WORK_PATH_HTML = 'https://chaucyrio.000webhostapp.com' . $_DOSSIER_TRAVAIL;					// Chemin à utiliser pour accéder à des fichiers en HTML (images, etc.)


// Variables globales pour les chemins (à utiliser pour inclure des fichiers depuis d'autres pages)
$GLOBALS['_DOSSIER_TRAVAIL'] = $_DOSSIER_TRAVAIL;							// Dossier relatif du projet
$GLOBALS['_WORK_PATH_PHP'] = $_WORK_PATH_PHP;								// Chemin pour include en PHP
$GLOBALS['_WORK_PATH_HTML'] = $_WORK_PATH_HTML;								// Chemin pour include en HTML



/*
*	Fichier nécessaires au fonctionnement des modules
*/
// require_once($_WORK_PATH_PHP . 'class/ChaussureManager.php');				// Base de données


// On démarre la session
session_start();

// Frameworks
// CDN ou en local ?
$mode = 'LOCAL';
switch ($mode) {
	case 'CDN':
		echo '<!-- Mode CDN -->
<!-- JQuery v3.3.1 -->
<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous">
</script>
<!-- Popper.js v1.12.9 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- BootStrap v4.1.0 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<!-- Tooltip -->
<script type="text/javascript" src="./js/tooltip.js"></script>
<script type="text/javascript" src="./js/ajax/badge.js"></script>';
		break;

	case 'LOCAL':
		echo '<!-- Mode Local -->
<!-- JQuery v3.3.1 -->
<script src="./library/jquery-3.3.1.min.js"></script>
<!-- Popper.js v1.12.9 -->
<script src="./library/popper.min.js"></script>
<!-- BootStrap v4.4.1 -->
<link rel="stylesheet" href="./bootstrap-4.4.1/css/bootstrap.min.css">
<script src="./bootstrap-4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/tooltip.js"></script>
<script type="text/javascript" src="./js/ajax/badge.js"></script>';
		break;
	
}
echo '
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">';

?>
